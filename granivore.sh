#/bin/bash

# This is for scanning non-git directories. For scanning git, use the gitleaks config files in this repo.

# Usage: ./granivore.sh dir1

this_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

for wordlist in "${this_dir}"/bips/bip-0039/*.txt; do

  # It takes 20 seconds to scan like 74KB of code with all the wordlists. Just going to scan for english ones right now.
  if [[ "${wordlist}" =~ "english" ]]; then

    # Build a regex like '(word1|word2|word3|...)'
    regex_words=$(echo "($(cat "${wordlist}" | xargs | tr ' ' '|';))")

    # TODO: Improve delimiter. Right now this just checks for space, tab, newline, and carriage return. But it's possible that someone could have used pipes, commas, or something else entirely...
    grep -rIiE "${regex_words}([ \t\n\r]+${regex_words}){11,23}" "${@}"

  fi

done
