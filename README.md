# Granivore

A granivore is an animal that finds and eats seeds.

This repo has gitleaks configs for finding cryptocurrency (e.g., Bitcoin, Ethereum, etc.) seed phrases, also known as mnemonic phrases.

It's a fairly CPU intense scan and can be slow.

I use it like this:

```
docker run -v ~/gitlab.com/evanstucker/granivore/gitleaks-english.toml:/gitleaks.toml -v "${PWD}":/path zricethezav/gitleaks:latest detect -s /path -v -c /gitleaks.toml
```
