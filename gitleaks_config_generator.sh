#/bin/bash

this_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

for wordlist in "${this_dir}"/bips/bip-0039/*.txt; do

  language=$(basename "${wordlist}" | sed 's/\.txt$//')

  # Build a regex like '(word1|word2|word3|...)'
  regex_words=$(echo "($(cat "${wordlist}" | xargs | tr ' ' '|';))")

  cat <<EOF > "${this_dir}/gitleaks-${language}.toml"
[[rules]]
description = "BIP39 Mnemonic (${language})"
id = "bip39-mnemonic-${language}"
regex = '''${regex_words}([ \t\n\r]+${regex_words}){11,23}'''
[rules.allowlist]
paths = [
    '''.*wordlist.*''',
]
EOF

done
